(
    media/kodi[~scm]
    media/mpv[~scm]
    media-libs/babl[~scm]
    media-libs/gegl[~scm]
    media-libs/libcamera[~scm]
    media-libs/libmypaint[~scm]
    media-libs/opus[~scm]
    media-libs/zxing-cpp[~scm]
    media-sound/mpc[~scm]
    media-sound/mpd[~scm]
    media-sound/ncmpcpp[~scm]
    net-misc/youtube-dl[~scm]
) [[
    *author = [ Paul Seidler <sepek@exherbo.org> ]
    *date = [ 06 Dec 2011 ]
    *token = scm
    *description = [ Mask scm versions ]
]]

media-libs/taglib[<1.12] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 16 Feb 2021 ]
    token = security
    description = [ CVE-2018-11439 ]
]]

media-libs/libraw[<0.21.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Dec 2023 ]
    token = security
    description = [ CVE-2023-1729 ]
]]

media-libs/libexif[<0.6.22-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 03 Dec 2020 ]
    token = security
    description = [ CVE-2020-0198, CVE-2020-0452 ]
]]

media-gfx/gimp[<2.8.22-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Jan 2018 ]
    token = security
    description = [ CVE-2017-1778{4,5,6,7,8,9} ]
]]

media-libs/OpenJPEG:0[<1.5.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 25 Aug 2014 ]
    token = security
    description = [ Multiple CVEs, see NEWS for details ]
]]

media-libs/lcms2[<2.8-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 25 Jan 2017 ]
    token = security
    description = [ CVE-2016-10165 ]
]]

media-libs/flac[<1.3.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 21 Feb 2022 ]
    token = security
    description = [ CVE-2020-0499, CVE-2021-0561 ]
]]

graphics/exiv2[<0.28.3] [[
    author = [ David Legrand <david.legrand@clever-cloud.com> ]
    date = [ 10 Jul 2024 ]
    token = security
    description = [ CVE-2024-39695 ]
]]

media/ffmpeg[<4.4-r3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Aug 2021 ]
    token = security
    description = [ CVE-2021-38291 ]
]]

media-libs/libsndfile[<1.0.31-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Aug 2021 ]
    token = security
    description = [ CVE-2021-3246 ]
]]

media-libs/giflib[<5.2.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Feb 2024 ]
    token = security
    description = [ CVE-2021-40633, CVE-2022-28506, CVE-2023-48161 ]
]]

media-gfx/optipng[<0.7.8] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Nov 2023 ]
    token = security
    description = [ CVE-2023-43907 ]
]]

media-libs/imlib2[<1.4.9] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 17 May 2016 ]
    token = security
    description = [ CVE-2011-5326, CVE-2016-{3993,3994,4024} ]
]]

media/vlc[<3.0.21] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Jun 2024 ]
    token = security
    description = [ VideoLAN-SB-VLC-3021 ]
]]

media-libs/libass[<0.15.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Oct 2020 ]
    token = security
    description = [ CVE-2020-26682 ]
]]

media-plugins/gst-plugins-bad:1.0[<1.18.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Jan 2021 ]
    token = security
    description = [ CVE-2021-3185 ]
]]

media-libs/libwebp[<1.3.1-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 13 Sep 2023 ]
    token = security
    description = [ CVE-2023-4863 ]
]]

media-libs/opus[<1.1.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Jan 2017 ]
    token = security
    description = [ CVE-2017-0381 ]
]]

media-libs/audiofile[<0.3.6-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Jan 2017 ]
    token = security
    description = [ CVE-2015-7747 ]
]]

media-gfx/icoutils[<0.31.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Mar 2017 ]
    token = security
    description = [ CVE-2017-60{09,10,11} ]
]]

media-gfx/sane-backends[<1.0.27] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2017-6318 ]
]]

media/kodi[<17.3-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Jul 2017 ]
    token = security
    description = [ CVE-2012-6706 ]
]]

media-sound/mpg123[<1.25.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Jul 2017 ]
    token = security
    description = [ CVE-2017-11126 ]
]]

media-sound/vorbis-tools[<1.4.2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 24 Jan 2021 ]
    token = security
    description = [ CVE-2015-6749, CVE-2014-9639 ]
]]

media-sound/lame[<3.100] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 18 Oct 2017 ]
    token = security
    description = [ CVE-2017-15045, CVE-2017-15046 ]
]]

media-libs/libvorbis[<1.3.6-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Dez 2019 ]
    token = security
    description = [ CVE-2017-14160, CVE-2018-10392, CVE-2018-10393 ]
]]

media/plex-media-server-bin[>1.13.5.5332] [[
    author = [ Rasmus Thomsen <cogitri@exherbo.org> ]
    date = [ 23 Aug 2018 ]
    token = testing
    description = [ Subscriber-only versions ]
]]

media/mpv[<0.33.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Apr 2021 ]
    token = security
    description = [ CVE number pending ]
]]

media-libs/lensfun[~>0.3.95] [[
    author = [ Bernd Steinhauser <berniyh@exherbo.org> ]
    date = [ 21 Sep 2018 ]
    token = pre-release
    description = [ Pre-release version ]
]]

media-libs/live[<20210814] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Aug 2021 ]
    token = security
    description = [ CVE-2021-38381 ]
]]

media-video/mkvtoolnix[<28.2.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Oct 2018 ]
    token = security
    description = [ CVE-2018-4022 ]
]]

media-libs/SDL_image:2[<2.0.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Nov 2018 ]
    token = security
    description = [ CVE-2018-3977 ]
]]

media-libs/OpenJPEG:2[<2.4.0-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Apr 2022 ]
    token = security
    description = [ CVE-2021-3575, CVE-2021-29338, CVE-2022-1122 ]
]]

media-libs/libheif[<1.17.6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Dec 2023 ]
    token = security
    description = [ CVE-2023-49462, CVE-2023-49463 ]
]]

media-libs/faad2[<2.11.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 08 Nov 2023 ]
    token = security
    description = [ CVE-2023-38857, CVE-2023-38858 ]
]]

media-libs/faac[<1.30] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Oct 2019 ]
    token = security
    description = [ CVE-2018-19886 ]
]]

media-libs/libmp4v2[<2.0.0-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Nov 2019 ]
    token = security
    description = [ CVE-2018-14054, CVE-2018-14325, CVE-2018-14326, CVE-2018-14379,
                    CVE-2018-14403, CVE-2018-14446 ]
]]

media-gfx/jhead[<3.08] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Oct 2023 ]
    token = security
    description = [ CVE-2020-{6625,6624,28840,26208},
                    CVE-2021-{3496,2827{5..8},34055},
                    CVE-2022-{41751,28550} ]
]]

media-libs/exempi[<2.5.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Oct 2020 ]
    token = security
    description = [ CVE-2018-12648 ]
]]

media-gfx/gifsicle[<1.94] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Oct 2023 ]
    token = security
    description = [ CVE-2023-36193 ]
]]

media-libs/openexr[<3.2.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 13 Sep 2024 ]
    token = security
    description = [ CVE-2024-31047 ]
]]

media-sound/wavpack[<5.5.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 13 Nov 2021 ]
    token = security
    description = [ CVE-2021-44269 ]
]]

media-libs/libebml[<1.4.2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 19 Feb 2021 ]
    token = security
    description = [ CVE-2021-3405 ]
]]

media-plugins/gst-plugins-good:1.0[<1.18.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 28 May 2021 ]
    token = security
    description = [ CVE-2021-3497, CVE-2021-3498 ]
]]

media-gfx/gimp[~>2.99.6] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 12 Aug 2021 ]
    token = pre-release
    description = [ Development version ]
]]

media-libs/SDL_ttf:2[<2.0.18] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2022 ]
    token = security
    description = [ CVE-2022-27470 ]
]]

media/vlc[>=4.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Aug 2022 ]
    token = testing
    description = [ Development version with support for FFmpeg 5.x ]
]]

media-libs/OpenImageIO[<2.4.10.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 11 Apr 2023 ]
    token = security
    description = [ CVE-2023-22845, CVE-2023-24472, CVE-2023-24473 ]
]]

media-libs/libde265[<1.0.15] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Dec 2023 ]
    token = security
    description = [ CVE-2023-49465, CVE-2023-49467, CVE-2023-49468 ]
]]

media-libs/opusfile[<0.12-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Feb 2023 ]
    token = security
    description = [ CVE-2022-47021 ]
]]

net-misc/yt-dlp[<2024.7.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Jul 2024 ]
    token = security
    description = [ CVE-2024-38519 ]
]]

media-libs/opencv[<4.8.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Sep 2023 ]
    token = security
    description = [ CVE-2023-2617, CVE-2023-2618 ]
]]

media-libs/libvpx[<1.14.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 Jul 2024 ]
    token = security
    description = [ CVE-2024-5197 ]
]]

media-libs/libcue[<2.2.1-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Oct 2023 ]
    token = security
    description = [ CVE-2023-43641 ]
]]

media-libs/libpano13[<2.9.22] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 31 Oct 2023 ]
    token = security
    description = [ CVE-2021-20307, CVE-2021-33293, CVE-2021-33798 ]
]]

media-libs/dav1d[<1.4.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Feb 2024 ]
    token = security
    description = [ CVE-2024-1580 ]
]]

app-text/calibre[<7.16.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Aug 2024 ]
    token = security
    description = [ CVE-2024-{6781,6782,7008,7009} ]
]]

(
    media-libs/libjxl:0.9[<0.9.4]
    media-libs/libjxl:0.10[<0.10.4]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 28 Nov 2021 ]
    *token = security
    *description = [ CVE-2024-11403, CVE-2024-11498 ]
]]

net-apps/snapcast[<0.30.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Jan 2025 ]
    token = security
    description = [ CVE-2023-36177 ]
]]

media-libs/openh264[<2.6.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Feb 2025 ]
    token = security
    description = [ CVE-2025-27091 ]
]]

media-libs/SDL_sound:2[<2.0.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Mar 2025 ]
    token = security
    description = [ CVE-2023-456{76,77,{79..82}} ]
]]
