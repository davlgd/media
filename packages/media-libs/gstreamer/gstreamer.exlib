# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require bash-completion
require meson

export_exlib_phases src_prepare src_test

SUMMARY="A multimedia framework with a plugin based architecture"
HOMEPAGE="https://${PN}.freedesktop.org"

LICENCES="LGPL-2"
SLOT="1.0"
DOWNLOADS="${HOMEPAGE}/src/${PN}/${PNV}.tar.xz"

DEPENDENCIES="
    post:
        app-admin/eclectic-gstreamer:${SLOT}
"

gstreamer_src_prepare() {
    meson_src_prepare

    # Test timeout expired
    edo sed -i -e '/tcase_add_test.*test_notify_race/d' tests/check/elements/fakesink.c
    edo sed -i -e '/tcase_add_test.*test_simple_shutdown_while_running_ring/d'                      \
               -e '/tcase_add_test.*test_simple_shutdown_while_running_ringbuffer/d'                \
            tests/check/elements/queue2.c

    # don't bind to 0.0.0.0
    edo sed -i -e '/net_time_provider_new/s/NULL/"127.0.0.1"/' tests/check/libs/gstnettimeprovider.c
    edo sed -e '/tcase_add_test (tc_chain, test_instantiation)/d' -i tests/check/libs/gstnetclientclock.c
    edo sed -e '/tcase_add_test (tc_chain, test_functioning)/d' -i tests/check/libs/gstnetclientclock.c
}

gstreamer_src_test() {
    # needed for GstNetClientClock tests
    esandbox allow_net --connect "inet:127.0.0.1@1024-65535"

    meson_src_test

    esandbox disallow_net --connect "inet:127.0.0.1@1024-65535"
}

