# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require github [ user=kcat project=${PN}-soft ] cmake

SUMMARY="A cross-platform software implementation of the OpenAL 3D audio API"
DESCRIPTION="
This library is meant as a compatible update/replacement to the OpenAL Sample
Implementation (the SI). The SI has been unmaintained for quite a while, and
would require a lot of work to clean up. After attempting to work on the SI for
a bit, I became overwhelmed with the amount of work needed, and I eventually
decided to fork the old Windows version to attempt an accelerated ALSA version.
The accelerated ALSA idea quickly fell through, but I ended up porting the
software mixing code to be cross-platform, with multiple output backends: ALSA,
OSS, DirectSound, and a .wav writer are currently implemented.

OpenAL Soft supports mono, stereo, 4-channel, 5.1, 6.1, and 7.1 output, as
opposed to the SI's 4-channel max (though it did have some provisions for 6
channel, this was not 5.1, and was seemingly a 'late' addition). OpenAL Soft
does not support the Vorbis and MP3 extensions, however those were considered
deprecated even in the SI. It does, though, support some of the newer
extensions like AL_EXT_FLOAT32 and AL_EXT_MCFORMATS for multi-channel and
floating-point formats, as well as ALC_EXT_EFX for environmental audio effects,
and others.
"
HOMEPAGE+=" https://openal-soft.org"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    alsa [[ description = [ Support alsa audio output ] ]]
    jack [[ description = [ Support jack audio output ] ]]
    pipewire [[ description = [ Support pipewire audio output ] ]]
    pulseaudio [[ description = [ Support pulseaudio audio output ] ]]
    sdl
    sndio [[ description = [ Support sndio audio output ] ]]
"

DEPENDENCIES="
    build:
        sys-kernel/linux-headers
        virtual/pkg-config
    build+run:
        alsa? ( sys-sound/alsa-lib )
        jack? ( media-sound/jack-audio-connection-kit )
        pipewire? ( media/pipewire[>=0.3.23] )
        pulseaudio? ( media-sound/pulseaudio )
        sdl? ( media-libs/SDL:2 )
        sndio? ( sys-sound/sndio )
    test:
        dev-cpp/gtest
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-Use-system-GTest.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DALSOFT_BACKEND_{OSS,WAVE}:BOOL=TRUE
    -DALSOFT_BACKEND_{COREAUDIO,OBOE,OPENSL,PORTAUDIO,SOLARIS}:BOOL=FALSE
    -DALSOFT_DLOPEN:BOOL=TRUE
    -DALSOFT_EXAMPLES:BOOL=FALSE
    -DALSOFT_INSTALL:BOOL=TRUE
    -DALSOFT_INSTALL_AMBDEC_PRESETS:BOOL=TRUE
    -DALSOFT_INSTALL_CONFIG:BOOL=TRUE
    -DALSOFT_INSTALL_HRTF_DATA:BOOL=TRUE
    # Only relevant if we'd disable ALSOFT_NO_CONFIG_UTIL
    #-DALSOFT_INSTALL_UTILS:BOOL=TRUE
    -DALSOFT_NO_CONFIG_UTIL:BOOL=TRUE
    -DALSOFT_REQUIRE_{COREAUDIO,OBOE,OPENSL,PORTAUDIO,SOLARIS}:BOOL=FALSE
    -DALSOFT_REQUIRE_OSS:BOOL=TRUE
    -DALSOFT_REQUIRE_RTKIT:BOOL=FALSE
    -DALSOFT_RTKIT:BOOL=FALSE
    -DALSOFT_SEARCH_INSTALL_DATADIR:BOOL=FALSE
    -DALSOFT_UTILS:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
    # libmysofa - "simple set of C functions to read AES SOFA files, if they
    # contain HRTFs stored according to the AES69-2015"
    -DCMAKE_DISABLE_FIND_PACKAGE_MySOFA:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    "alsa ALSOFT_BACKEND_ALSA"
    "alsa ALSOFT_REQUIRE_ALSA"
    "jack ALSOFT_BACKEND_JACK"
    "jack ALSOFT_REQUIRE_JACK"
    "pipewire ALSOFT_BACKEND_PIPEWIRE"
    "pipewire ALSOFT_REQUIRE_PIPEWIRE"
    "pulseaudio ALSOFT_BACKEND_PULSEAUDIO"
    "pulseaudio ALSOFT_REQUIRE_PULSEAUDIO"
    "sdl ALSOFT_BACKEND_SDL2"
    "sdl ALSOFT_REQUIRE_SDL2"
    "sndio ALSOFT_BACKEND_SNDIO"
    "sndio ALSOFT_REQUIRE_SNDIO"
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DALSOFT_TESTS:BOOL=TRUE -DALSOFT_TESTS:BOOL=FALSE'
)

src_install() {
    cmake_src_install

    insinto /etc/xdg
    hereins alsoft.conf <<EOF
[general]
drivers=pipewire,pulse,alsa,oss
EOF
}

pkg_postinst() {
    local cruft=( /etc/openal/alsoft.conf )
    for file in ${cruft[@]}; do
        if test -f "${file}" ; then
            nonfatal edo rm "${file}" || ewarn "removing ${file} failed"
        fi
    done
}

