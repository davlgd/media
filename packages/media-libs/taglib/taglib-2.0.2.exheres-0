# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'taglib-1.5.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require github [ tag=v${PV} ] cmake
require alternatives

SUMMARY="A library for reading and editing audio meta data"
HOMEPAGE+=" https://${PN}.github.io"

LICENCES="|| ( LGPL-2.1 MPL-1.1 )"
# TAGLIB_SOVERSION_MAJOR
SLOT="2"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        dev-libs/utfcpp
        virtual/pkg-config
        doc? ( app-doc/doxygen[dot] )
    build+run:
        sys-libs/zlib
    run:
        !media-libs/taglib:0[<1.13.1-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
    test:
        dev-cpp/cppunit
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DBUILD_BINDINGS:BOOL=TRUE
    -DBUILD_EXAMPLES:BOOL=FALSE
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DENABLE_CCACHE:BOOL=FALSE
    -DPLATFORM_WINRT:BOOL=FALSE
    -DWITH_ZLIB:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

src_compile() {
    cmake_src_compile

    option doc && ecmake_build --target docs
}

src_install() {
    local local arch_dependent_alternatives=()
    local host=$(exhost --target)

    cmake_src_install

    if option doc ; then
        insinto /usr/share/doc/${PNVR}/
        doins -r doc/*
    fi

    arch_dependent_alternatives+=(
        /usr/${host}/bin/${PN}-config         ${PN}-config-${SLOT}
        /usr/${host}/include/${PN}            ${PN}-${SLOT}
        /usr/${host}/lib/libtag.so            libtag-${SLOT}.so
        /usr/${host}/lib/libtag_c.so          libtag_c-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc   ${PN}-${SLOT}.pc
        /usr/${host}/lib/pkgconfig/${PN}_c.pc ${PN}_c-${SLOT}.pc
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

