# Copyright 2009 Maxime Coste <frrrwww@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require SDL_lib

SUMMARY="Image file loading library for SDL"

LICENCES="ZLIB"
SLOT="$(ever range 1)"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    tiff
    webp
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        media-libs/SDL:0[>=1.2.10]
        media-libs/libpng:=
        !media-libs/SDL_image:0 [[
            description = [ Uninstall media-libs/SDL_image:0 after switching to the slotted version ]
            resolution = uninstall-blocked-after
        ]]
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        tiff? ( media-libs/tiff:= )
        webp? ( media-libs/libwebp:= )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # link to the graphics libraries instead of dlopening them at runtime
    --disable-jpg-shared
    --disable-png-shared
    --disable-tif-shared
    --disable-webp-shared
    --enable-jpg
    --enable-png
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'tiff tif'
    webp
)

