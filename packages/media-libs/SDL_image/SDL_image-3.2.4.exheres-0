# Copyright 2009 Maxime Coste <frrrwww@gmail.com>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PNV/SDL/SDL3}

require github [ user=libsdl-org project=${PN} release=release-${PV} suffix=tar.gz ]
require cmake

SUMMARY="Image decoding for many popular formats for Simple Directmedia Layer"

LICENCES="ZLIB"
SLOT="$(ever range 1)"
PLATFORMS="~amd64 ~armv7 ~x86"
MYOPTIONS="
    avif [[ description = [ Support for the AV1 Image File Format ] ]]
    jpegxl
    tiff
    webp
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        media-libs/SDL:3
        media-libs/libpng:=
        sys-libs/zlib
        avif? ( media-libs/libavif:=[>=1.0] )
        jpegxl? ( media-libs/libjxl:= )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        tiff? ( media-libs/tiff:= )
        webp? ( media-libs/libwebp:= )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DSDLIMAGE_BACKEND_STB:BOOL=FALSE
    # link to the graphics libraries instead of dlopening them at runtime
    -DSDLIMAGE_DEPS_SHARED:BOOL=FALSE
    -DSDLIMAGE_INSTALL:BOOL=TRUE
    -DSDLIMAGE_INSTALL_CPACK:BOOL=TRUE
    -DSDLIMAGE_INSTALL_MAN:BOOL=FALSE
    -DSDLIMAGE_SAMPLES:BOOL=FALSE
    -DSDLIMAGE_SAMPLES_INSTALL:BOOL=FALSE
    -DSDLIMAGE_STRICT:BOOL=TRUE
    -DSDLIMAGE_TESTS_INSTALL:BOOL=FALSE
    -DSDLIMAGE_VENDORED:BOOL=FALSE
    -DSDLIMAGE_WERROR:BOOL=FALSE

    -DSDLIMAGE_BMP:BOOL=TRUE
    -DSDLIMAGE_GIF:BOOL=TRUE
    -DSDLIMAGE_JPG:BOOL=TRUE
    -DSDLIMAGE_JPG_SAVE:BOOL=TRUE
    -DSDLIMAGE_LBM:BOOL=TRUE
    -DSDLIMAGE_PCX:BOOL=TRUE
    -DSDLIMAGE_PNM:BOOL=TRUE
    -DSDLIMAGE_PNG:BOOL=TRUE
    -DSDLIMAGE_PNG_SAVE:BOOL=TRUE
    -DSDLIMAGE_QOI:BOOL=TRUE
    -DSDLIMAGE_SVG:BOOL=TRUE
    -DSDLIMAGE_TGA:BOOL=TRUE
    -DSDLIMAGE_XCF:BOOL=TRUE
    -DSDLIMAGE_XPM:BOOL=TRUE
    -DSDLIMAGE_XV:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'avif SDLIMAGE_AVIF'
    'avif SDLIMAGE_AVIF_SAVE'
    'jpegxl SDLIMAGE_JXL'
    'tiff SDLIMAGE_TIF'
    'webp SDLIMAGE_WEBP'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DSDLIMAGE_TESTS:BOOL=TRUE -DSDLIMAGE_TESTS:BOOL=FALSE'
)

