# Copyright 2014 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=AcademySoftwareFoundation tag=v${PV} ] cmake

SUMMARY="A library for reading and writing images"

LICENCES="
    BSD-3
    MIT   [[ note = [ bundled pugixml ] ]]
    GPL-2 [[ note = [ bundled tbb ] ]]
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    freetype [[ description = [ Support for rendering text on image buffers ] ]]
    gif
    heif [[ description = [ Support for the H(igh) E(fficiency) I(mage) F(ile) F(ormat) ] ]]
    jpeg2000
    raw [[ description = [ Enable reading RAW image files from digital photo cameras ] ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# 34 tests failed out of 120, last checked: 2.4.6.0
RESTRICT="test"

DEPENDENCIES="
    build:
        app-text/txt2man
        dev-libs/robin-map
    build+run:
        dev-libs/boost[>=1.66]
        dev-libs/fmt[>=7.0]
        dev-libs/pugixml[>=1.8]
        media-libs/OpenColorIO[>=2.2]
        media-libs/imath
        media-libs/libpng:=
        media-libs/libwebp:=
        media-libs/openexr[>=3.1]
        media-libs/tiff:=[>=4.0]
        sys-libs/zlib
        freetype? ( media-libs/freetype:2 )
        gif? ( media-libs/giflib:=[>=5.0] )
        heif? ( media-libs/libheif[>=1.16] )
        jpeg2000? ( media-libs/OpenJPEG:2[>=2.2] )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo[>=2.1] )
        raw? ( media-libs/libraw[>=0.18] )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_DOCS:BOOL=TRUE
    -DBUILD_FMT_FORCE:BOOL=FALSE
    -DBUILD_MISSING_FMT:BOOL=FALSE
    -DBUILD_MISSING_ROBINMAP:BOOL=FALSE
    -DBUILD_OIIOUTIL_ONLY:BOOL=FALSE
    -DBUILD_ROBINMAP_FORCE:BOOL=FALSE
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DCLANG_TIDY:BOOL=FALSE
    -DCMAKE_INSTALL_DOCDIR:PATH=/usr/share/doc/${PNVR}
    -DCMAKE_INSTALL_MANDIR:PATH=/usr/share/man/man1
    -DCODECOV:BOOL=FALSE
    -DEMBEDPLUGINS:BOOL=TRUE
    -DEXTRA_WARNINGS:BOOL=FALSE
    -DINSTALL_DOCS:BOOL=TRUE
    -DINSTALL_FONTS:BOOL=TRUE
    -DINTERNALIZE_FMT:BOOL=FALSE
    -DLINKSTATIC:BOOL=FALSE
    -DOIIO_BUILD_TOOLS=TRUE
    -DOIIO_DISABLE_BOOST_STACKTRACE:BOOL=FALSE
    -DOIIO_DISABLE_BUILTIN_OCIO_CONFIGS:BOOL=FALSE
    -DOIIO_DOWNLOAD_MISSING_TESTDATA:BOOL=FALSE
    -DOIIO_NAMESPACE_INCLUDE_PATCH:BOOL=FALSE
    -DOIIO_TEX_IMPLEMENT_VARYINGREF:BOOL=TRUE
    -DOIIO_THREAD_ALLOW_DCLP:BOOL=TRUE
    -DOIIO_USE_EXR_C_API:BOOL=TRUE
    -DOPENIMAGEIO_CONFIG_DO_NOT_FIND_IMATH:BOOL=FALSE
    -DPYLIB_INCLUDE_SONAME:BOOL=FALSE
    -DPYLIB_LIB_PREFIX:BOOL=FALSE
    -DSTOP_ON_WARNING:BOOL=FALSE
    -DTIME_COMMANDS:BOOL=FALSE
    -DUSE_CCACHE:BOOL=FALSE
    -DUSE_DCMTK:BOOL=FALSE
    -DUSE_EXTERNAL_PUGIXML:BOOL=TRUE
    -DUSE_FFMPEG:BOOL=FALSE
    -DUSE_GENERATED_EXPORT_HEADER:BOOL=FALSE
    -DUSE_LIBCPLUSPLUS:BOOL=FALSE
    -DUSE_NUKE:BOOL=FALSE
    -DUSE_OPENCOLORIO:BOOL=TRUE
    -DUSE_OPENCV:BOOL=FALSE
    -DUSE_OPENGL:BOOL=FALSE
    -DUSE_OPENVDB:BOOL=FALSE
    -DUSE_PTEX:BOOL=FALSE
    -DUSE_PYTHON:BOOL=FALSE
    -DUSE_QT:BOOL=FALSE
    -DUSE_R3DSDK:BOOL=FALSE
    -DUSE_TBB:BOOL=FALSE
    -DVERBOSE:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_USES=(
    FREETYPE
    GIF
    'heif LIBHEIF'
    'jpeg2000 OPENJPEG'
    'providers:jpeg-turbo JPEGTURBO'
    'raw LIBRAW'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
    '-DOIIO_BUILD_TESTS:BOOL=TRUE -DOIIO_BUILD_TESTS:BOOL=FALSE'
)

