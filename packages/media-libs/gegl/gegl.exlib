# Copyright 2009 Richard Brown <rbrown@exherbo.org>
# Copyright 2011, 2012 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm; then
    require gnome.org
else
    DOWNLOADS="https://download.gimp.org/pub/${PN}/$(ever range 1-2)/${PNV}.tar.xz"
fi

require meson
require alternatives
# ff-load-save tests segfault with ffmpeg >= 6
require ffmpeg [ with_opt=true abis=[ 4 5 ] ]
require vala [ vala_dep=true with_opt=true ]

export_exlib_phases pkg_setup src_prepare src_configure src_compile src_install

SUMMARY="GEGL (Generic Graphics Library) is a graph based image processing framework"
DESCRIPTION="
GEGL provides infrastructure to do demand based cached non destructive image editing
on larger than RAM buffers. Through babl it provides support for a wide range of color
models and pixel storage formats for input and output.
"
HOMEPAGE="https://${PN}.org"

UPSTREAM_RELEASE_NOTES="https://gitlab.gnome.org/GNOME/${PN}/blob/master/docs/NEWS.txt"

LICENCES="GPL-3 LGPL-3"
MYOPTIONS="
    exif ffmpeg gobject-introspection gtk-doc jpeg2000 lcms openmp openexr pdf
    raw sdl svg tiff
    v4l  [[ description = [ V4L2 device support ] ]]
    webp
    ( x86_cpu_features: mmx sse )
    ( platform: amd64 )
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# TODO: -Ddocs=true has automagic deps upon asciidoc & ruby
# Also, they always rebuild and only install if the tools above are present.
# See:  https://bugzilla.gnome.org/show_bug.cgi?id=676688
DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config
        gtk-doc? (
            dev-doc/gi-docgen
            dev-doc/gtk-doc
        )
    build+run:
        core/json-glib[>=1.2.6]
        dev-libs/glib:2[>=2.44.0]
        media-libs/babl[$(ever is_scm && echo '~scm' || echo '>=0.1.78')][gobject-introspection?]
        media-libs/libpng:=[>=1.6.0]
        sys-libs/zlib[>=1.2.0]
        x11-libs/cairo[>=1.12.2]
        x11-libs/gdk-pixbuf:2.0[>=2.32.0]
        x11-libs/pango[>=1.38.0]
        exif? ( dev-libs/gexiv2[>=0.14.0] )
        gobject-introspection? (
            gnome-bindings/pygobject:3[>=3.2]
            gnome-desktop/gobject-introspection:1[>=1.32.0]
        )
        jpeg2000? ( media-libs/jasper[>=1.900.1] )
        lcms? ( media-libs/lcms2[>=2.8] )
        openexr? ( media-libs/openexr[>=1.6.1] )
        openmp? ( sys-libs/libgomp:= )
        pdf? ( app-text/poppler[>=0.71.0][glib] )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo[>=1.0.0] )
        sdl? ( media-libs/SDL:2[>=2.0.5] )
        svg? ( gnome-desktop/librsvg:2[>=2.40.6] )
        raw? ( media-libs/libraw[>=0.15.4] )
        tiff? ( media-libs/tiff:=[>=4.0.0] )
        v4l? ( media-libs/v4l-utils[>=1.0.1] )
        webp? ( media-libs/libwebp:=[>=0.5.0] )
        !media-libs/gegl:0[<=0.2.0-r7] [[
            description = [ /usr/bin/gegl collides ]
            resolution = upgrade-blocked-before
        ]]
    test:
        gobject-introspection? (
            dev-lang/python:*[>=2.5]
        )
"

gegl_pkg_setup() {
    meson_pkg_setup
    ffmpeg_pkg_setup
    vala_pkg_setup
}

gegl_src_prepare() {
    meson_src_prepare

    # The tests is a bit flaky and times out, sometimes
    edo sed -e "/backend-file/d" -e "/gegl-node/d" -i tests/simple/meson.build
}

gegl_src_configure() {
    local myconf=(
        -Ddocs=false
        -Dcairo=enabled
        -Dgdk-pixbuf=enabled
        -Dpango=enabled
        -Dpangocairo=enabled
        -Dgraphviz=disabled
        -Dlensfun=disabled
        -Dlibspiro=disabled
        -Dlua=disabled
        -Dmaxflow=disabled # unpackaged
        # mrg and gexiv2 needed for gui
        -Dmrg=disabled # unpackaged
        -Dsdl1=disabled
        -Dumfpack=disabled
        $(meson_switch gobject-introspection introspection)
        $(meson_switch gtk-doc)
        $(meson_feature exif gexiv2)
        $(meson_feature ffmpeg libav)
        $(meson_feature gtk-doc gi-docgen)
        $(meson_feature jpeg2000 jasper)
        $(meson_feature lcms)
        $(meson_feature openexr)
        $(meson_feature openmp)
        $(meson_feature pdf poppler)
        $(meson_feature gobject-introspection pygobject)
        $(meson_feature raw libraw)
        $(meson_feature sdl sdl2)
        $(meson_feature svg librsvg)
        $(meson_feature tiff libtiff)
        $(meson_feature v4l libv4l)
        $(meson_feature v4l libv4l2)
        $(meson_feature vapi vapigen)
        $(meson_feature webp)
    )

    exmeson "${myconf[@]}"
}

gegl_src_compile() {
    # For some unknown reason dependencies between targets aren't right on
    # musl... Force generation of headers to work around that.
    if [[ $(exhost --target) == *-musl* ]] ; then
        for file in "${MESON_SOURCE}"/opencl/*.cl ; do
            edo ninja opencl/${file##*/}.h
        done
    fi

    meson_src_compile
}

gegl_src_install() {
    meson_src_install

    local alternatives=(
        /usr/$(exhost --target)/bin/${PN} ${PN}-${SLOT}
        /usr/$(exhost --target)/bin/${PN}-imgcmp ${PN}-imgcmp-${SLOT}
    )

    alternatives_for _${PN} ${PN}-${SLOT} ${SLOT} "${alternatives[@]}"
}

