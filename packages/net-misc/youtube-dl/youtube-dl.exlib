# Copyright 2010-2015 Johannes Nixdorf <mixi@shadowice.org>
# Distributed under the terms of the GNU General Public License v2

require bash-completion zsh-completion
require github [ user=rg3 release=${PV} suffix=tar.gz ]
require setup-py [ import=setuptools has_bin=true multibuild=false ]

export_exlib_phases src_prepare src_install

SUMMARY="A small command-line program to download videos from youtube.com or other video platforms"

LICENCES="Unlicense"
SLOT="0"
MYOPTIONS=""

# they need internet access
RESTRICT="test"

#    test:
#        dev-python/nose
DEPENDENCIES="
    suggestion:
        media/ffmpeg:* [[
            description = [ Many extractors have FFmpeg as a requirement ]
        ]]
"

if ! ever is_scm; then
    WORK=${WORKBASE}/${PN}
fi

BASH_COMPLETIONS=( ${PN}.bash-completion )
ZSH_COMPLETIONS=( "${PN}.zsh _${PN}" )

youtube-dl_src_prepare() {
    setup-py_src_prepare

    # don't install etc/bash_completion.d into /usr
    # don't install fish completion in /usr/etc/...
    # don't install README.txt to /usr/share/doc/${PNV}
    # (it's installed by emagicdocs to the right place)
    edo sed \
        -e '/bash_completion/d' \
        -e '/youtube-dl.fish/d' \
        -e '/README.txt/d' \
        -i setup.py
}

youtube-dl_src_install() {
    setup-py_src_install

    if ever is_scm ; then
        edo rmdir "${IMAGE}"/usr/share/man/man1 "${IMAGE}"/usr/share/man
    fi

    bash-completion_src_install
    zsh-completion_src_install
}

