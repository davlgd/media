# Copyright 2020 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cargo [ rust_minimum_version="1.70.0" disable_default_features=true ]
require github [ user=xiph tag=v${PV} ]

SUMMARY="The fastest and safest AV1 encoder"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    ( platform: amd64 x86 )
"

DEPENDENCIES="
    build:
        dev-rust/cargo-c[>=0.9.27] [[ note = [ just the upstream CI version ] ]]
        platform:amd64? ( dev-lang/nasm[>=2.14.02] )
        platform:x86? ( dev-lang/nasm[>=2.14.02] )
"

ECARGO_FEATURES=(
    # Avoid git_version, which depends on git2 and thus libgit2, but fails to
    # build dues some undefined reference error. It's only used to get better
    # version information with non-release builds.
    binaries asm threading signal_support
)

src_compile() {
    cargo_src_compile
    ecargo cbuild --release --frozen \
        --prefix=/usr/$(exhost --target)
}

src_install() {
    cargo_src_install
    ecargo cinstall --release --frozen \
        --prefix=/usr/$(exhost --target) \
        --destdir="${IMAGE}"
}

