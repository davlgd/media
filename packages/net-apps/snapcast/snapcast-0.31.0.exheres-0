# Copyright 2016-2025 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SNAPWEB_VER="0.8.0"

require github [ user=badaix tag=v${PV} ] \
    cmake \
    systemd-service

SUMMARY="Multi-room client-server audio player"
DESCRIPTION="
Snapcast is a multiroom client-server audio player, where all clients are time synchronized with
the server to play perfectly synced audio. It's not a standalone player, but an extension that
turns your existing audio player into a Sonos-like multiroom solution. Audio is captured by the
server and routed to the connected clients. Several players can feed audio to the server in
parallel and clients can be grouped to play the same audio stream. One of the most generic ways to
use Snapcast is in conjunction with the music player daemon (MPD) or Mopidy.
"
DOWNLOADS+=" https://github.com/badaix/snapweb/releases/download/v${SNAPWEB_VER}/snapweb.zip -> snapweb-${SNAPWEB_VER}.zip"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    avahi
    flac
    jack
    opus [[ description = [ Adds support for the Opus audio codec ] ]]
    pulseaudio
    vorbis
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/boost[>=1.74.0]
        virtual/pkg-config
    build+run:
        group/audio
        group/snapserver
        user/snapclient
        user/snapserver
        dev-libs/expat
        media-libs/soxr
        sys-sound/alsa-lib
        avahi? ( net-dns/avahi[dbus] )
        flac? ( media-libs/flac:= )
        jack? ( media-sound/jack-audio-connection-kit )
        opus? ( media-libs/opus )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        pulseaudio? ( media-sound/pulseaudio )
        vorbis? (
            media-libs/libogg
            media-libs/libvorbis
        )
    test:
        dev-cpp/catch[>=3]
    suggestion:
        dev-python/dbus-python [[
            description = [ Required for the MPD stream controlscript plugin (meta_mpd.py) ]
        ]]
        dev-python/musicbrainzngs [[
            description = [ Required for the MPD stream controlscript plugin (meta_mpd.py) ]
        ]]
        dev-python/python-mpd2 [[
            description = [ Required for the MPD stream controlscript plugin (meta_mpd.py) ]
        ]]
        dev-python/requests [[
            description = [ Required for the librespot java stream controlscript plugin (meta_librespot-java.py) ]
        ]]
        dev-python/websocket-client [[
            description = [ Required for the Mopidy and librespot java stream controlscript plugin (meta_mopidy.py/meta_librespot-java.py) ]
        ]]
        dev-rust/librespot[>=0.1.0] [[
            description = [ Spotify Connect support via librepot ]
        ]]
        gnome-bindings/pygobject:3 [[
            description = [ Required for the MPD stream controlscript plugin (meta_mpd.py) ]
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DASAN:BOOL=FALSE
    -DBUILD_CLIENT:BOOL=TRUE
    -DBUILD_SERVER:BOOL=TRUE
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DBUILD_STATIC_LIBS:BOOL=FALSE
    -DBUILD_WITH_EXPAT:BOOL=TRUE
    -DBUILD_WITH_TREMOR:BOOL=FALSE
    -DCMAKE_INSTALL_SYSCONFDIR:PATH=/etc
    -DTIDY:BOOL=FALSE
    -DTSAN:BOOL=FALSE
    -DUBSAN:BOOL=FALSE
    -DWERROR:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS=(
    'avahi WITH_AVAHI'
    'flac WITH_FLAC'
    'jack WTTH_JACK'
    'opus WITH_OPUS'
    'pulseaudio WITH_PULSE'
    'vorbis WITH_VORBIS'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE'
)

src_prepare() {
    cmake_src_prepare

    edo sed \
        -e 's:/etc/default/snapclient:/etc/conf.d/snapclient.conf:g' \
        -e 's:Group=snapclient:Group=audio:g' \
        -i extras/package/debian/snapclient.service

    edo sed \
        -e 's:/etc/default/snapserver:/etc/conf.d/snapserver.conf:g' \
        -i extras/package/debian/snapserver.service
}

src_install() {
    cmake_src_install

    doman "${CMAKE_SOURCE}"/server/snapserver.1
    doman "${CMAKE_SOURCE}"/client/snapclient.1

    insinto /etc
    doins "${CMAKE_SOURCE}"/server/etc/snapserver.conf

    insinto /etc/conf.d
    newins "${CMAKE_SOURCE}"/extras/package/debian/snapserver.default snapserver.conf
    newins "${CMAKE_SOURCE}"/extras/package/debian/snapclient.default snapclient.conf

    insinto ${SYSTEMDSYSTEMUNITDIR}
    doins "${CMAKE_SOURCE}"/extras/package/debian/snap{client,server}.service

    keepdir /etc/snapserver/certs
    keepdir /var/lib/snap{client,server}
    edo chown -R snapserver:snapserver "${IMAGE}"/var/lib/snapserver
    edo chown -R snapclient:audio "${IMAGE}"/var/lib/snapclient

    # snapweb
    insinto /usr/share/snapserver/snapweb
    doins "${WORKBASE}"/*.*
    doins -r "${WORKBASE}"/assets
}

